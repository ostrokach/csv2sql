CSV2SQL
=======

CSV2SQL is a tool to simplify the loading of text files into relational databases.

It analyses a text file and spits out SQL that creates a database table and loads data into that table.


